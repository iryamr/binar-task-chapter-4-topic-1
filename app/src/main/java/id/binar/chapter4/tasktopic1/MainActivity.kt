package id.binar.chapter4.tasktopic1

import addAction
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import id.binar.chapter4.tasktopic1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        showToast()
        showSnackbar()
    }

    private fun showToast() {
        binding.btnToast.setOnClickListener {
            if (binding.etName.text.toString().isEmpty()) {
                binding.etlName.error = getString(R.string.field_name_cannot_be_empty)
                return@setOnClickListener
            }

            createToast(getString(R.string.text_toast, binding.etName.text.toString()), Toast.LENGTH_LONG)
        }
    }

    private fun createToast(text: String, duration: Int) = Toast.makeText(this, text, duration).show()

    private fun showSnackbar() = binding.btnSnackbar.setOnClickListener { createSnackbar(it, getString(R.string.text_snackbar)) }

    private fun createSnackbar(view: View, message: String): Snackbar {
        return Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE).also { snackbar ->
            snackbar.setAction(getString(R.string.text_action_1)) {
                createToast(getString(R.string.text_toast_action_1), Toast.LENGTH_SHORT)
            }
            snackbar.addAction(R.layout.snackbar_extra_button, getString(R.string.text_action_2)) {
                createToast(getString(R.string.text_toast_action_2), Toast.LENGTH_SHORT)
            }
            snackbar.show()
        }
    }
}